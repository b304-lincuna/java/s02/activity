package com.zuitt.WDC043_S2_A1;

import java.util.Scanner;

public class LeapYear {
    public static void main(String[] args){

        Scanner getYear = new Scanner(System.in);

        System.out.println("Input year to be checked if a leap year: ");
        int intYear = getYear.nextInt();

        boolean isLeapYear = (intYear % 4 == 0 && intYear % 100 != 0 || intYear % 400 == 0);

        if (isLeapYear){
            System.out.println(intYear + " is a leap year.");
        }else{
            System.out.println(intYear + " is NOT a leap year.");
        }


    }
}
