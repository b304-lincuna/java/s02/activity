package com.zuitt.WDC043_S2_A2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class PrimeNumber {
    public static void main(String[] args){


        int[] intArray= new int[5];

        intArray[0]= 2;
        intArray[1]= 4;
        intArray[2]= 6;
        intArray[3]= 8;
        intArray[4]= 10;

        System.out.println("The first prime number is: "+intArray[0]);

        //====================================================


        ArrayList<String> friends = new ArrayList<>(Arrays.asList("John","Jane","Chloe","Zoey"));

        System.out.println("My friends are: "+ friends);

        //===================================================

        HashMap<String, Integer> inventory = new HashMap<>(){
            {
                put("toothpaste",15);
                put("toothbrush",20);
                put("soup",20);
            }
        };

        System.out.println("Our current inventory consists of: " + inventory );

    }
}
